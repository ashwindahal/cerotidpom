package FrontierAirline;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrontierAirlines {
	// Class Level Variable
	static WebDriver driver;

	public static void main(String[] args) {

		// 1: Invoke the browser
		invokeBrowser();

		// 2: Fill the form
		fillForm();

		// 3: Validate Page
		validatePage();

		// 4: Terminate the Browser
		terminate();

	}

	public static void terminate() {
		// Close the Browser
		driver.close();
		// Quite the driver
		driver.quit();

	}

	public static void validatePage() {

		// Add Wait for page to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// 1: validate text "ROUND-TRIP FARES"
		String textValue1 = "ROUND-TRIP FARES";
		String textToValidate1 = driver.findElement(By.xpath("(//div[contains(text(),'Round-trip Fares')])[1]"))
				.getText();

		if (textToValidate1.equalsIgnoreCase(textValue1)) {
			System.out.println("Pass: Expected Text '" + textValue1 + "' is visible in Page");
		} else {
			System.out.println("Failed: Expected Text is not visible in Page");
		}

		// 2: valiedate text "Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)"
		String textValue2 = "Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)";
		String textToValidate2 = driver
				.findElement(
						By.xpath("(//div[contains(text(),'Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)')])[2]"))
				.getText();

		if (textToValidate2.equalsIgnoreCase(textValue2)) {
			System.out.println("Pass: Expected Text '" + textValue2 + "' is visible in Page");
		} else {
			System.out.println("Failed: Expected Text is not visible in Page");
		}

	}

	public static void fillForm() {
		try {
			// 1: Close Cookies option
			WebElement cookieXBtn = driver.findElement(By.xpath("(//button[@aria-label='Close'])[3]"));
			cookieXBtn.click();

			// 2: From Element
			WebElement fromCity = driver.findElement(By.xpath("//input[@name='kendoDepartFrom_input']"));
			fromCity.sendKeys("DFW");
			fromCity.sendKeys(Keys.TAB);

			// Add Wait for page to load
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			// 3: To Element
			WebElement toCity = driver.findElement(By.xpath("//input[@name='kendoArrivalTo_input']"));
			toCity.sendKeys("LAX");
			// Add Wait for page to load
			Thread.sleep(5000);

			toCity.sendKeys(Keys.RETURN);

			// Add Wait for page to load
			Thread.sleep(5000);

			// 4: Calender Icon
			WebElement calenderIcon = driver.findElement(By.xpath("//input[@id='departureDate']"));
			calenderIcon.click();

			// 5: Selecting Departure and Return Dates
			WebElement departureDate = driver.findElement(By.xpath("//a[@id='7-15-2020']"));
			departureDate.click();

			Thread.sleep(5000);

			// Store the current window
			String firstWindow = driver.getWindowHandle();
			// Click close when new window opens
			for (String windows : driver.getWindowHandles()) {
				driver.switchTo().window(windows);
			}
			driver.close();
			driver.switchTo().window(firstWindow);

			WebElement returnDate = driver.findElement(By.xpath("//a[@id='7-22-2020']"));
			returnDate.click();

			// clciking on Travelers Field
			WebElement travelersField = driver.findElement(By.xpath("//input[@id='passengersInput']"));
			travelersField.click();

			WebElement addAdult = driver.findElement(By.xpath("(//img[@role='button'])[2]"));
			addAdult.click();

			// Add waite
			Thread.sleep(5000);

			// Travelers Field click
			travelersField.sendKeys(Keys.RETURN);

			// Search Btn
			WebElement searchBtn = driver.findElement(By.xpath("(//img[@class='SearchSVG'])[1]"));
			searchBtn.click();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

	}

	public static void invokeBrowser() {
		// 1: set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Chrome driver obj
		driver = new ChromeDriver();

		// Navigate to Frontier Website
		driver.get("https://www.flyfrontier.com/");

		if (driver.getTitle().contains("Low Fares Done Right | Frontier Airlines")) {
			System.out.println(driver.getTitle() + "-------------------------------- Was Launched");
		}

		// Max the screen
		driver.manage().window().maximize();
		// Add Wait for page to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
