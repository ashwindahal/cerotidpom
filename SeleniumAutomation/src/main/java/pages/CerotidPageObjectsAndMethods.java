package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectsAndMethods {

	// Creating a empty webdriver
	WebDriver driver = null;

	// Locating Elements using BY Object
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By fullName = By.xpath("//input[@placeholder='Full Name *']");
	By addressElement = By.xpath("// input[@placeholder='Address *']");
	By cityElement = By.xpath("//input[@id='city']");
	By stateElement = By.xpath("//select[@id='state']");
	By zipCode = By.xpath("//input[@id='zip']");
	By email = By.xpath("//input[@placeholder='Email *']");
	By phoneElement = By.xpath("//input[@placeholder='Phone *']");
	By visa = By.xpath("//select[@id='visaStatus']");
	By relocateNo = By.xpath("//input[@value='N0']");
	By educationDetails = By.xpath("//textarea[@id='eduDetails']");
	By submitBtn = By.xpath("//button[@id='registerButton']");

	// Creating Consstructor so we can utilize the web driver
	public CerotidPageObjectsAndMethods(WebDriver driver) {
		this.driver = driver;
	}

	// Creating methods that let me interact with elments

	// Select course
	public void selectCourse(String course) {
		// Creating new webelement obj and passing By(select course) as a argument
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(course);
	}

	public void selectSession(String session) {
		WebElement element = driver.findElement(selectSession);
		Select chooseSession = new Select(element);
		chooseSession.selectByVisibleText(session);
	}

	public void enterFullName(String name) {
		driver.findElement(fullName).sendKeys(name);
	}

	public void enterAddress(String address) {
		driver.findElement(addressElement).sendKeys("123 Test Address");
	}

	public void enterCity(String city) {
		driver.findElement(cityElement).sendKeys(city);
	}

	public void chooseState(String state) {
		WebElement element = driver.findElement(stateElement);
		Select selectSate = new Select(element);
		selectSate.selectByVisibleText(state);
	}

	public void enterZip(String zip) {
		if (isElementPresent(zipCode)) {
			System.out.println("Element Zip Code Found");
			driver.findElement(zipCode).sendKeys(zip);
		} else {
			System.out.println("Element not located");
		}

	}

	public void enterPhoneNum(String phone) {
		driver.findElement(phoneElement);
	}

	// Frame Work Method (check if a elelemt is present or not)
	public boolean isElementPresent(By by) {
		boolean check = false;
		try {
			driver.findElement(by);
			check = true;
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
			e.getCause();
			check = false;
		}
		return check;
	}
}
