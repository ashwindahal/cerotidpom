package pages;
//Ashwin Nepal: This class will hold Web elemnts from cerotid and help us understand Page object model 

//This class will return elements to class CerotidPageTest 

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CerotidPage {

	// Class level Variable to store web element value
	private static WebElement element = null;

	// Course element
	public static WebElement selectCourse(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;
	}

	// Session Date
	public static WebElement selectSession(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
	}

	// Full Name Element
	public static WebElement fullName(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		return element;

	}

	// Address Element
	public static WebElement addressField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("// input[@placeholder='Address *']"));
		return element;

	}

	// City Element
	public static WebElement cityField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@id='city']"));
		return element;
	}

	// State Element
	public static WebElement stateSelect(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;

	}

	// State Element
	public static WebElement zipField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@id='zip']"));
		return element;

	}

	// email Element
	public static WebElement emailField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Email *']"));
		return element;
	}

	// Phone Element
	public static WebElement phoneField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Phone *']"));
		return element;
	}

	// Visa Element
	public static WebElement visaStatus(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;
	}

	// Visa Element
	public static WebElement mediaSource(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;
	}

	// Relocate NO Element
	public static WebElement relocateNoBtn(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@value='N0']"));
		return element;
	}

	// Education details text area Element
	public static WebElement eduTextArea(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;
	}

	// Submit button Element
	public static WebElement submitBtn(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//button[@id='registerButton']"));
		return element;
	}

}
