package LinerAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) throws InterruptedException {

		// 1. : Setting the System path - providing location of the chromedriver

		// Windows
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Mac
		// System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");

		// 2. Creating new chromedriver object
		WebDriver driver = new ChromeDriver();

		// 3. invoke the brower/ navigate to google.com
		driver.get("https://www.Google.com");

		// 4. Maxmize the browser
		driver.manage().window().maximize();

		// Validate that the title is "Google"
		if (driver.getTitle().contains("Google")) {
			System.out.println("Passed- Navigated to Valid page");
		} else {
			System.out.println("Failed- invalid Page");
		}

		// Perfrom some actions on the UI / Webpage
		// Test Case: Navigate to Google.com and search for what is selenium

		// 1. Find the Elements needed to interact with the UI for our Test case
		// Note: Two ways to interact with elements on a page
		// a: Absolute xpath -- It contains the complete path from the Root Element to
		// the desire element.

		// b. Relative Xpath -- This is more like starting simply by referencing the
		// element you want and go from the particular location.
		// xpath -- search field //input[@name='q']
		// // --> Current Node
		// input --> tagname
		// @--> slects attribute
		// name --> attribute name
		// q --> value of the attribute

		// Sending Keys to search bar
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		txtBoxSearch.sendKeys("What is selenium");

		// Pressing Enter
		txtBoxSearch.sendKeys(Keys.RETURN);

		// Implicite Waite
		TimeUnit.SECONDS.sleep(5);

		String titleAfterSearch = driver.getTitle();

		// Validate after search
		if (titleAfterSearch.contains("What is selenium - Google Search")) {
			System.out.println("Passed- Navigated to Valid page");
		} else {
			System.out.println("Failed- invalid Page");

		}
		
		// Close will terminate tab
		driver.close();

		// Terminate the browser
		driver.quit();

	}
}
