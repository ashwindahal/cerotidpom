package LinerAutomation;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinerExample {

	public static void main(String[] args) throws InterruptedException {
		// Step 1: Invoke Browser: Navigate to the Cerotid Page

		// a. Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// b. Create a Chromedriver Object
		WebDriver driver = new ChromeDriver();

		// c. Navigate to cerotid page

		// c.1 Get url
		// driver.get("http://www.cerotid.com");

		// c.2 Navigate to url
		driver.navigate().to("http://www.cerotid.com");

		// d. maximize window
		driver.manage().window().maximize();

		// Step 2: Fill Form in Cerotid Page

		// a. Created a Webelement object
		WebElement selectCourse = driver.findElement(By.xpath("//select[@id='classType']"));
		// b. Create a select obj and pass the element we want to select
		Select chooseCourse = new Select(selectCourse);
		// c. creating a string variable with course name
		String courseName = "QA Automation";
		// d. selecting the course by visible text
		chooseCourse.selectByVisibleText(courseName);

		// Create a Session type Weblement Object
		WebElement sessionType = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(sessionType);
		String session = "Upcoming Session";
		chooseSession.selectByVisibleText(session);

		// Create Full Name Webelement
		WebElement fullName = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		fullName.sendKeys("Test Tester");

		// Creating a adress Webelement
		WebElement addressField = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		addressField.sendKeys("75045 tester dr, Irving Tx");

		// Creating city Webelement
		WebElement cityField = driver.findElement(By.xpath("//input[@id='city']"));
		cityField.sendKeys("75045 tester dr, Irving Tx");

		// Find all elelemts inside the state dropdown and choose the state to be
		// slected

		// Creating Object in java

		// Locating the elements and storing them in a list
		List<WebElement> listOfStates = driver.findElements(By.xpath("//select[@id='state']/option"));
		// Looping through the list of webelements
		Iterator<WebElement> elementIterrator = listOfStates.iterator();
		// Click a element during iteration
		while (elementIterrator.hasNext()) {
			WebElement element = elementIterrator.next();
			// Check to see if a element exist
			if (element.getText().contains("TX")) {
				element.click();
			}
		}

		// Creating zipcode element
		WebElement zipCode = driver.findElement(By.xpath("//input[@id='zip']"));
		zipCode.sendKeys("75159");

		WebElement emailField = driver.findElement(By.xpath("(//input[@id='email'])[1]"));
		emailField.sendKeys("test@tester.com");

		WebElement phoneField = driver.findElement(By.xpath("(//input[@id='phone'])[1]"));
		phoneField.sendKeys("1234567899");

		WebElement visaStatus = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		Select chooseVisa = new Select(visaStatus);
		String visa = "USCitizen";
		chooseVisa.selectByVisibleText(visa);

		WebElement mediaSource = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		Select chooseMediaSource = new Select(mediaSource);
		String media = "Social Media";
		chooseMediaSource.selectByVisibleText(media);

		// Clicking on the No Radio Button
		// Create WebElement Object
		WebElement ableToRelocateNOBtn = driver.findElement(By.xpath("//input[@value='N0']"));
		ableToRelocateNOBtn.click();

		WebElement eduDetails = driver.findElement(By.xpath("// textarea[@id='eduDetails']"));
		eduDetails.sendKeys("This is a automation test");

		WebElement submitBtn = driver.findElement(By.xpath("//button[@id='registerButton']"));
		submitBtn.click();

		Thread.sleep(5000);

		// Step 3: Validate Success Message
		WebElement sucessMessage = driver.findElement(By.xpath("//div[@class='alert alert-success']/strong"));

		if (sucessMessage.getText().contains("Your register is completed. We will contact you shortly!")) {
			System.out.println("Valid Message is Displayed");
		} else {
			System.out.println("Valid Message is not displayed as expected");
		}

	}

}
