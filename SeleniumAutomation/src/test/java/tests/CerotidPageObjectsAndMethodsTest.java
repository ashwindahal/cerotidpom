package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPageObjectsAndMethods;

public class CerotidPageObjectsAndMethodsTest {

	public static WebDriver driver = null;

	public static void main(String[] args) {
		// 1: invoke browser
		invokeBrowser();

		// 2: fillForm
		fillForm();

		// 3: validateSuccessMessage
		// validateSucessMessage();

		// 4: Terminate the Brower
		// terminateBrowser();

	}

	// Step 1
	public static void invokeBrowser() {
		// 1: set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Chrome driver obj
		driver = new ChromeDriver();

		// Navigate to Frontier Website
		driver.get("http://www.cerotid.com");

		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "-------------------------------- Was Launched");
		}

		// Max the screen
		driver.manage().window().maximize();
		// Add Wait for page to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// Step 2
	public static void fillForm() {
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		cerotidPageObj.selectCourse("Java");
		cerotidPageObj.selectSession("Upcoming Session");
		cerotidPageObj.enterFullName("Test Name");
		cerotidPageObj.enterAddress("123 some test address");
		cerotidPageObj.enterCity("sunnyvale");
		cerotidPageObj.chooseState("CA");
		cerotidPageObj.enterZip("12345");

	}

	// Step 3
	public static void validateSucessMessage() {

		// Expected message to check
		String expectedMessage = "Your register is completed. We will contact you shortly!";

		// Actual Message from UI
		String actualMessage = driver.findElement(By.xpath("//strong[contains(text(),'Your register is completed.')]"))
				.getText();

		// Checking if messages are as expected
		if (expectedMessage.equalsIgnoreCase(actualMessage)) {
			System.out.println("Pass: Expected Message " + expectedMessage + " was displayed");
		} else {
			System.out.println("Failed: Expected Message " + expectedMessage + "was NOT displayed as expected");
			System.out.println("Actual Message: " + actualMessage);
		}

	}

	// Step 4
	public static void terminateBrowser() {
		// Close will terminate tab
		driver.close();
		// Terminate the browser
		driver.quit();
	}

}
