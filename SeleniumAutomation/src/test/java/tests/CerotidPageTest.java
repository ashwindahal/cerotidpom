package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidPage;

public class CerotidPageTest {

	private static WebDriver driver = null;

	public static void main(String[] args) {
		// 1: invoke browser
		invokeBrowser();	
		// 2: fill form
		fillForm();
		// 3: Validate Success Message
		validateSucessMessage();
		// 4: Terminate browser
		terminateBrowser();

	}

	public static void terminateBrowser() {
		// Close will terminate tab
		driver.close();
		// Terminate the browser
		driver.quit();
	}

	public static void validateSucessMessage() {

		// Expected message to check
		String expectedMessage = "Your register is completed. We will contact you shortly!";

		// Actual Message from UI
		String actualMessage = driver.findElement(By.xpath("//strong[contains(text(),'Your register is completed.')]"))
				.getText();

		// Checking if messages are as expected
		if (expectedMessage.equalsIgnoreCase(actualMessage)) {
			System.out.println("Pass: Expected Message " + expectedMessage + " was displayed");
		} else {
			System.out.println("Failed: Expected Message " + expectedMessage + "was NOT displayed as expected");
			System.out.println("Actual Message: " + actualMessage);
		}

	}

	public static void fillForm() {
		try {
			// Utilizing the CerotidPage Objects/Elements to slelect course
			Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
			chooseCourse.selectByVisibleText("QA Automation");

			// Utilizing the CerotidPage Objects/Elements to slelect session
			Select chooseSession = new Select(CerotidPage.selectSession(driver));
			chooseSession.selectByVisibleText("Upcoming Session");

			// Entering name in full name field
			CerotidPage.fullName(driver).sendKeys("Test");

			// Entering address in address field
			CerotidPage.addressField(driver).sendKeys("123 Test Address");

			// Entering City
			CerotidPage.cityField(driver).sendKeys("Sunnyvale");

			// Selecting state
			Select chooseState = new Select(CerotidPage.stateSelect(driver));
			chooseState.selectByVisibleText("CA");

			// Zip field
			CerotidPage.zipField(driver).sendKeys("12345");

			// Email field
			CerotidPage.emailField(driver).sendKeys("test@test.com");

			// Phone Field
			CerotidPage.phoneField(driver).sendKeys("1234567890");

			// Visa Status
			Select visaStatus = new Select(CerotidPage.visaStatus(driver));
			visaStatus.selectByVisibleText("OPT");

			// Media Source
			Select mediaSource = new Select(CerotidPage.mediaSource(driver));
			mediaSource.selectByVisibleText("Social Media");

			// Clicking on No
			CerotidPage.relocateNoBtn(driver).click();

			// Entering text in education text field
			CerotidPage.eduTextArea(driver).sendKeys("This is a automated test");

			// Click on submit
			CerotidPage.submitBtn(driver).click();

		} catch (Exception e) {
			e.getStackTrace();
			e.getMessage();
		}

	}

	public static void invokeBrowser() {
		// 1: set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Chrome driver obj
		driver = new ChromeDriver();

		// Navigate to Frontier Website
		driver.get("http://www.cerotid.com");

		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "-------------------------------- Was Launched");
		}

		// Max the screen
		driver.manage().window().maximize();
		// Add Wait for page to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
